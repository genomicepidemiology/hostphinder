# README #

These are the HostPhinder scripts used by the HostPhinder web service available here:
https://cge.cbs.dtu.dk/services/HostPhinder/

The programming language is python and the scripts can be downloaded and used to
create the same results as the web service in combination with HostPhinders
databases available here:
SHOULD THE DATABASE BE MADE AVAILABLE ON THE CGE DATASETS PAGE?

Please note that you need a python installation with all the required packages
to run the scripts, we recommend using anaconda (http://continuum.io/downloads)
which has all dependencies covered. Also change the first line in the scripts to
point to your installation of python or anaconda if necessary.

When using HostPhinder please cite:
TO BE ADDED...

HOW TO RUN HostPhinder COMMANDLINE:---------------------------------------------

There are two scripts: findtemplate_scipy.py which searches the phage database
using a kmer-approach and get_host_alpha.py which finds the best host predictions
and outputs a list of these with the best host at the top.

example of how to run findtemplate_scipy.py:------------------------------------

the program has the following options:

-i inputfile in FASTA format (or raw reads)
-t kmer-database name (including location i.e. path to database)
-o outputfile
-k kmersize (this must correspond to the kmersize that was used to create the kmer-database)
-p pickleinput (use this option if the database is pickled)
-e e-value

findtemplate_scipy.py -i infile.fna -t path/to/db/dbname -o output.txt -k 16 -p -e 0.05

If you are using the same database as the HostPhinder web service there are two
sets of databases, one for species level host predictions and one for genus
level host predictions. For species level predictions replace dbname with
accnwhost1871.list_step1_kmer16_thres1.0_db, for genus level host predictions
replace dbname with accnwhost2196.list_step1_kmer16_thres1.0_db.



example of how to run get_host_alpha.py:----------------------------------------

After running findtemplate_scipy.py you can submit the output you obtained to
get_host_alpha.py to obtain the ordered list of host predictions.
The script has the following options:

-f inputfile (findtemplate_scipy.py output)
-d decision (after which column should results be sorted? default is 'coverage')
-a alpha default is 6.0
-m meta data file (containing host association)
-l level of prediction (species or genus)
-i infilename (name of original input file to findtemplate_scipy.py)
-o outputfile (the program will also create a file named output file_top_hit
containing the best host prediction)

get_host_alpha.py -f output.txt -m metadata -l species -i infile.fna -o pred_hosts.txt

If you are using the same database as the HostPhinder web service you should use
meta_1871_species_150506.tab for predictions on species level and
meta_2196_genus_150506.tab for predictions on genus level as the metadata file.
