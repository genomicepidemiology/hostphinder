#!/usr/bin/env python

# Julia Villarroel
# last modified: 150219

# Vanessa Jurtz, last modified 150713

import argparse
import sys
from compiler.ast import flatten
import os
import re

parser= argparse.ArgumentParser(description='Get prediction and result from ALL table for best result.')

parser.add_argument('-f','--file', type=str, help="File that contains results with host SORTED by user decision")
parser.add_argument('-d','--decision', type=str, default='coverage', help='Value which should be chosen to find majority host: Score, z, frac_q, frac_d or coverage')
parser.add_argument('-a','--alpha', type=float, default=6.0, help='alpha value')
parser.add_argument('-m', '--metafile', type=str, help='File with metadata (host specifications)')
parser.add_argument('-l', '--level', type=str, help='Level of prediction: genus, species')
parser.add_argument('-o', '--outfile', type=str, help='File to store output')
parser.add_argument('-i','--infilename', type=str, help="Name of original FASTA input file")


args = parser.parse_args()

if args.metafile != None:
    metafile=open(args.metafile,"r")
else:
    sys.stderr.write("Please specify meta data file!\n")
    sys.exit(2)

if args.outfile != None:
    outfile=open(args.outfile,"w")
    outfile_top=open(args.outfile + "_top_hit","w")
else:
    sys.stderr.write("Please specify output file!\n")
    sys.exit(2)

if args.level != None:
    level=args.level
else:
    sys.stderr.write("Please specify level of prediction: species, genus\n")
    sys.exit(2)

if args.infilename != None:
    infilename=args.infilename.split("/")[-1]
else:
    sys.stderr.write("Please specify name of original FASTA input file!\n")
    sys.exit(2)

get_col = {
	'Score' : 1,
	'z' : 3,
	'frac_q' : 5,
	'frac_d' : 6,
	'coverage' : 7
}

if not args.file:
	sys.stderr.write('WARNING: Please specify the sorted results file with hosts!\n')
	sys.exit(2)

#-------------------------------------------------------------------------------
#   Read metadata file
#-------------------------------------------------------------------------------
host_dict={}
for l in metafile:
    l=l.strip()
    l=l.split("\t")
    host_dict[l[0]]=l[-1]
metafile.close()


#------------------------------------------------------------------------------
#	Read predictions
#------------------------------------------------------------------------------
pred_tmp = [pred.strip().split('\t') for pred in open(args.file)
	if not pred.startswith('Template') | pred.startswith('#')]

#-------------------------------------------------------------------------------
#   check if findtemplate was able to predict hosts:
#-------------------------------------------------------------------------------

if len(pred_tmp)==0:
    if level=="species":
        outfile.write("#Query\tPredicted Host (species)\tCoverage\n")
        outfile_top.write("#Query\tPredicted Host (species)\tCoverage\n")
    elif level=="genus":
        outfile.write("#Query\tPredicted Host (genus)\tCoverage\n")
        outfile_top.write("#Query\tPredicted Host (genus)\tCoverage\n")
    else:
        sys.stderr.write("Unknown level: " + level + " allowed: species genus")
        sys.exit(2)
    # report that no hit was found:
    outfile.write(infilename + "\tno hit found\t0\n")
    outfile_top.write(infilename + "\tno hit found\t0\n")

    outfile.close()
    outfile_top.close()

    #exit programm:
    sys.stderr.write("DONE!")
    sys.exit(0)

#-------------------------------------------------------------------------------
#   store host in description column
#-------------------------------------------------------------------------------

for hit in pred_tmp:
    hit[-1]=host_dict[hit[0].strip()]


#-------------------------------------------------------------------------------
#   sort on user selection
#-------------------------------------------------------------------------------

pred=sorted(pred_tmp, key=lambda x: x[get_col[args.decision]], reverse=True)


#------------------------------------------------------------------------------
#	frac threshold
#------------------------------------------------------------------------------
# First hit value
first_value = float(pred[0][get_col[args.decision]])


# Make host => [score, dec_value] dictionary
host_set= set()
for hit in pred:
	host_set.add(hit[-1])


hosts = {k: [0, 0] for k in host_set}

for hit in pred:
	score = (float(hit[get_col[args.decision]]) / first_value)**args.alpha
	hosts[hit[-1]][0] += score
	dec_value = hit[get_col[args.decision]]
	# Take the measure value of the highest hit
	if hosts[hit[-1]][1] == 0:
		hosts[hit[-1]][1] = dec_value

# Take host with highest value
winner = sorted(host_set, key=lambda x: (hosts[x][0], hosts[x][1]),
	 reverse=True)#[0]
# for host in winner:
#   print '%s\t%s\t%s' % (args.file, host, hosts[host][1])

#-------------------------------------------------------------------------------
#   print results to outfile:
#-------------------------------------------------------------------------------

# Query Predicted Host (species/genus) coverage
if level=="species":
    outfile.write("#Query\tPredicted Host (species)\tCoverage\n")
    outfile_top.write("#Query\tPredicted Host (species)\tCoverage\n")
elif level=="genus":
    outfile.write("#Query\tPredicted Host (genus)\tCoverage\n")
    outfile_top.write("#Query\tPredicted Host (genus)\tCoverage\n")
else:
    sys.stderr.write("Unknown level: " + level + " allowed: species genus")
    sys.exit(2)

count=0
for host in winner:
    outfile.write(infilename + "\t" + host + "\t" + str(hosts[host][1]) + "\n")

    if count < 1:
        outfile_top.write(infilename + "\t" + host + "\t" + str(hosts[host][1]) + "\n")
    count+=1
outfile.close()
outfile_top.close()

sys.stderr.write("DONE!")
